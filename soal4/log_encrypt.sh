#!/bin/bash

syslog="/var/log/syslog"
hour="$(date "+%H")" 
namefile="$(date "+%H:%M %d-%m-%Y").txt"

lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

#geser huruf sesuai ketentuan
lowercase="${lower[$hour]}"
uppercase="${upper[$hour]}"

backup="$(cat $syslog |tr '[a-z]' "[$lowercase-za-$lowercase]" | tr '[A-Z]' "[$uppercase-ZA-$uppercase]")"

#simpan file backup yang sudah dienkripsi
echo "$backup" >  /home/akmalx/Documents/"$namefile"

#crontab configuration
#0 */2 * * *  /bin/bash /home/akmalx/Praktikum_sisop/soal4/log_encrypt.sh
