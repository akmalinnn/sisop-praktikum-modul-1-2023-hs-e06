#!/bin/bash
#script sistem register

echo ===============REGISTER==================

echo Masukkan username:
    read uname
echo Masukkan password:
    read pass

#timestamp
TIMESTAMP=$(date +'%y/%m/%d %H:%M:%S')

#cek password
if [[ ${#pass} -ge 8 ]] && [[ "$pass" =~ [[:upper:]] ]] &&
[[ "$pass" =~ [[:lower:]] ]] && [[ "$pass" =~ ^[[:alnum:]]+$ ]] &&
[[ "$pass" != "$uname" ]] && [[ "$pass" != *"chicken"* ]] &&
[[ "$pass" != *"ernie"* ]]
then
    CARIUSER=$( grep -o "$uname" users/users.txt)
    
    if ["CARIUSER"]
    #user sudah ada
    then
        MESSAGE="REGISTER: ERROR User already exists"
    else
        MESSAGE="REGISTER: INFO User $uname registered successfully"
fi

echo "$MESSAGE"

#redirecting outputnya ke log.txt
echo "$TIMESTAMP $MESSAGE" >> log.txt