#!/bin/bash
#script sistem login

echo ===============LOGIN==================

echo Masukkan username:
    read uname
echo Masukkan password:
    read pass

#timestamp
TIMESTAMP=$(date +'%y/%m/%d %H:%M:%S')


if [ "$(grep "$uname $pass" users/users.txt)" ]
then
    MESSAGE="LOGIN: INFO User $uname logged in"
else
    MESSAGE="LOGIN: ERROR Failed login attempt on user $uname"
fi

echo "$MESSAGE"

#redirecting outputnya ke log.txt
echo "$TIMESTAMP $MESSAGE" >> log.txt