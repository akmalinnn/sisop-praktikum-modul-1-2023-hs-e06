#!/bin/bash

hour=$(date +"%H")

if [ "$hour" == "00" ]; then
    countx=1
else
    countx="$hour"
fi

nama_folder=$(ls -d kumpulan_* | tail -n 1 2>/dev/null || echo "kumpulan_0")
nama_file=$(ls -d perjalanan_* | tail -n 1 2>/dev/null || echo "perjalanan_0")

new_folder_name="kumpulan_$(($(echo "$nama_folder" | awk -F"_" '{print $2}')+1))"

mkdir "$new_folder_name"

for((i=1; i<=$countx;i++)) do

new_file_name="perjalanan_$(( $(echo "$nama_file" | awk -F"_" '{print $2}')+$i)).jpg"

wget -O "$new_folder_name/$new_file_name" "https://abrokenbackpack.com/wp-content/uploads/2020/02/temples-bali-1-min.jpg"
done

loc=./

zip_prefix=devil_

zip_count=$(ls -l $loc.zip | wc -l)

zip_count=$((zip_count+1))

zip_name=$zip_prefix$zip_count.zip

zip -r $zip_name $new_folder_name

