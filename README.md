# sisop-praktikum-modul-1-2023-HS-E06



## Praktikum Modul-1 Sistem Operasi


- 5025211049	Zakia Kolbi
- 5025211210	Nadiah Nuri Aisyah
- 5025211216	Akmal Nafis

# Nomor 1
Disedikan File 2023 QS World University Rankings.csv

- No 1 a, Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
```bash
awk -F',' '{gsub(/"/,"");if($3=="JP" && $4=="Japan") print $1,$2}' OFS='\t'  '2023 QS World University Rankings.csv' |head -n 5| sort -n
```
disini memakai awk dan `-F','` untuk membuat line separator dari file .csv yang tersedia, `gsub(/"/,"")` digunakan untuk menghilangkan tanda petik, pada semua data. Sedangkan `if($3=="JP" && $4=="Japan") print $1,$2}` digunakan untuk memfilter kolom ketiga dan keempat agar hanya menunjukkan lokasi Jepang, dan menuliskan rank dan nama universitas . OFS='\t' Digunakan untuk memberikan jarak pada setiap argumen yang diprint. Selanjutnya `head -n 5` dan `sort -n` digunakan untuk menampilkan 5 Universitas teratas dengan rank tertinggi   
- No 1 b,Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
```bash
awk -F',' '{gsub(/"/,"");if($3=="JP" && $4=="Japan" ) print $2,$9}' OFS='\t' '2023 QS World University Rankings.csv' |sort -t$'\t' -k2g |head -n 5
```
Menggunakan fungsi yang sama seperti no1 a untuk memfilter universitas yang ada di jepang, kemudian menuliskan nama universitas di kolom kedua dan fsr scorenya di kolom sembilan, `sort -t$'\t' -k2g` digunakan untuk mensorting, -t$'\t' untuk mensorting berdasarkan separator tiap argumen (menggunakan tab) `-k2g` untuk menunjukkan kolom kedua dari hasil print yakni angka fsr, dengan sorting secara general numerik dengan default dari kecil ke besar. `head -n 5` untuk menampilkan 5 Universitas dengan fsr terendah diseluruh Jepang

- no 1 c,Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi. 
```bash
awk -F',' '{gsub(/"/,"");if($3=="JP" && $4=="Japan") print $2,$20}' OFS='\t' '2023 QS World University Rankings.csv' |sort -t$'\t' -k2gr |head -n 10 
```
menggunakan sorting yang sama dengan no 2, tetapi dengan memperlihatkan ger rank tertinggi, `-k2gr` digunakan untuk mensorting kolom kedua setelah diprint yakni nilai ger , menggunakan -r karena ditampilkan yang nilainya paling tinggi. `head -n 10 ` untuk menampilkan hanya 10 teratas dari hasil sorting.

- no 1 d, Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
```bash
grep -E  'keren|Keren' '2023 QS World University Rankings.csv' | awk -F',' '{gsub(/"/,""); print $2}'
```
Kami menggunakan `grep -E` untuk mencari kata kunci `'keren|Keren'` yang memiliki dua opsi K besar atau kecil. Kemudian menggunakan fungsi awk print untuk menampilkan nama Universitas dengan kata kunci keren.

![image](https://cdn.discordapp.com/attachments/472360024975605762/1083569053198729288/image.png)

# Nomor 2
- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

```bash
Link gambar : https://abrokenbackpack.com/wp-content/uploads/2020/02/temples-bali-1-min.jpg

Untuk mendapatkan waktu saat ini dan menformat dalam jam dalam 24 jam

hour=$(date +"%H")
if [ "$hour" == "00" ]; 
then
    countx=1
else
    countx="$hour"
fi
```

```bash
Mengunduh gambar sebanyak waktu yang didapatkan lalu mengubah namanya menjadi perjalanan_1 dst

for((i=1; i<=$countx;i++)) do

new_file_name="perjalanan_$(( $(echo "$nama_file" | awk -F"_" '{print $2}')+$i)).jpg"

wget -O "$new_folder_name/$new_file_name" "https://abrokenbackpack.com/wp-content/uploads/2020/02/temples-bali-1-min.jpg"
```
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)

```bash
nama_file=$(ls -d perjalanan_* | tail -n 1 2>/dev/null || echo "perjalanan_0")
```
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 

```bash
nama_folder=$(ls -d kumpulan_* | tail -n 1 2>/dev/null || echo "kumpulan_0")

new_folder_name="kumpulan_$(($(echo "$nama_folder" | awk -F"_" '{print $2}')+1))"

mkdir "$new_folder_name"
```
- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas

```bash
loc=./
zip_prefix=devil_
zip_count=$(ls -l $loc.zip | wc -l)
zip_count=$((zip_count+1))
zip_name=$zip_prefix$zip_count.zip
zip -r $zip_name $new_folder_name
```
```bash
untuk konfigurasi cronjobnya sebagai berikut

0 */10 * * *  /home/maou/kobeni_liburan.sh
```

output 
![image](https://cdn.discordapp.com/attachments/472360024975605762/1083758551958294528/WhatsApp_Image_2023-03-10_at_21.27.56.jpg)
![image](https://cdn.discordapp.com/attachments/472360024975605762/1083758601144901704/WhatsApp_Image_2023-03-10_at_21.27.57.jpg)
# Nomor 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
Minimal 8 karakter
Memiliki minimal 1 huruf kapital dan 1 huruf kecil
Alphanumeric
Tidak boleh sama dengan username 
Tidak boleh menggunakan kata chicken atau ernie
```bash
#cek password
if [[ ${#pass} -ge 8 ]] && [[ "$pass" =~ [[:upper:]] ]] &&
[[ "$pass" =~ [[:lower:]] ]] && [[ "$pass" =~ ^[[:alnum:]]+$ ]] &&
[[ "$pass" != "$uname" ]] && [[ "$pass" != *"chicken"* ]] &&
[[ "$pass" != *"ernie"* ]]
```
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
```bash
#timestamp
TIMESTAMP=$(date +'%y/%m/%d %H:%M:%S')
```
```bash
#redirecting outputnya ke log.txt
echo "$TIMESTAMP $MESSAGE" >> log.txt
```
Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
```bash
CARIUSER=$( grep -o "$uname" users/users.txt)
    
    if [ "CARIUSER" ]
    #user sudah ada
    then
        MESSAGE="REGISTER: ERROR User already exists"
    else
        MESSAGE="REGISTER: INFO User $uname registered successfully"
        echo "$uname $pass" >> /users/users.txt
    fi
```
Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
```bash
if [ "$(grep "$uname $pass" /users/users.txt)" ]
then
    MESSAGE="LOGIN: INFO User $uname logged in"
else
    MESSAGE="LOGIN: ERROR Failed login attempt on user $uname"
fi

echo "$MESSAGE"

#redirecting outputnya ke log.txt
echo "$TIMESTAMP $MESSAGE" >> log.txt
```


# Nomor 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
```bash
syslog="/var/log/syslog"
hour="$(date "+%H")" 
namefile="$(date "+%H:%M %d-%m-%Y").txt"
```
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam 
`syslog="/var/log/syslog"` merupakan lokasi file syslog yang akan diambil,  
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam. `'$(date "+%H:%M %d-%m-%Y").txt"` merupakan format file yang akan disimpan 
```bash
hour="$(date "+%H")" 
```
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
untuk menyimpan jam sesuai dengan waktu pada komputer. 
- Menggunakan sistem cipher 

```bash
lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
lowercase="${lower[$hour]}"
uppercase="${upper[$hour]}"
```
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
Setelah huruf z akan kembali ke huruf a
Disini menggunakan array of char untuk menampung huruf besar ataupun kecil. `lowercase="${lower[$hour]}"` dan `uppercase="${upper[$hour]}"` digunakan untuk menggeser huruf sesuai jam yang ditentukan. 
- Setelah huruf z akan kembali ke huruf a
```bash
backup="$(cat $syslog |tr '[a-z]' "[$lowercase-za-$lowercase]" | tr '[A-Z]' "[$uppercase-ZA-$uppercase]")"

#simpan file backup yang sudah dienkripsi
echo "$backup" >  /home/akmalx/Documents/"$namefile"
```
untuk membaca file syslog disini digunakan cat `$syslog` yang berisi path file syslog, kemudian `tr '[a-z]' "[$lowercase-za-$lowercase]" | tr '[A-Z]' "[$uppercase-ZA-$uppercase]")"` dimana tr digunakan untuk memanipulasi huruf kecil dan besar dari a sampai z, sesuai dengan inisiasi $lowercase dan $uppercase, untuk mengembalikan z ke a digunakan `[z-a]`. 
- Buat juga script untuk dekripsinya.
```bash
ls "/home/akmalx/Documents/"
echo "Enter the file: "
read file
hour=${file:0:2}

lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
```
`ls` untuk menampilkan file hasil enkripsi sesuai dengan pathnya, kemudian menggunakan `read` untuk menerima inputan berupa nama file, kemudian variabel hour untuk menyimpan jam pada nama file tersebut. selanjutnya dibutuhkan variabel untuk menyimpan string berisi huruf besar dan kecil, urutan alfabet dituliskan sebanyak dua kali agar lebih presisi.

```bash
cat "/home/akmalx/Documents/$file" | tr "${lowercase:$hour:26}${uppercase:$hour:26}" "${lowercase:0:26}${uppercase:0:26}"  > "/home/akmalx/Documents/dekrip/$file"
echo "Decrypted file saved to /home/akmalx/Documents/dekrip/'$file' from /home/akmalx/Documents/'$file'"
```
- Buat juga script untuk dekripsinya.
`cat "/home/akmalx/Documents/$file"` untuk membaca file hasil enkripsi, `tr` untuk menterjemahkan setiap karakter huruf, ketika huruf sudah diterjemahkan maka huruf tersebut akan bergeser dengan format terbalik sesuai jam. Hasil dekripsi disimpan dengan path `"/home/akmalx/Documents/dekrip/$file"` menggunakan format nama yang sama.

- untuk konfigurasi cronjobnya sebagai berikut dimana log_encrypt.sh akan dieksekusi setiap 2 jam menggunakan bash
```bash
0 */2 * * *  /bin/bash /home/akmalx/Praktikum_sisop/soal4/log_encrypt.sh
```
Hasil enkripsi 
![image](https://cdn.discordapp.com/attachments/472360024975605762/1083738793095471196/image.png)

Jalankan dekripsi
![image](https://cdn.discordapp.com/attachments/472360024975605762/1083739035350077460/image.png)

Hasil dekripsi
![image](https://cdn.discordapp.com/attachments/472360024975605762/1083739437713862747/image.png)



# Hambatan dan Kesulitan Dalam Praktikum
Dalam pengerjaan soal kami masih kesulitan menentukan algoritma yang pas untuk menyelesaikan problem soal dengan syntax yang belum kami ketahui, terkadang bash script yang kami buat masih sering mengalami eror. 
